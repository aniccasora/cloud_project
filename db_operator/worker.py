# -*- coding:UTF-8 -*-
import boto3
from boto3.dynamodb.conditions import Key
from datetime import datetime
import decimal
# 排序用 西西
from boto3.dynamodb.conditions import Key, Attr
import json
from  DecimalEncoder import *

class Dynamodb_Worker:
    def __init__(self):

        # table name , 你資料表換了 這個也要跟著換
        self.table_name = "score_record"
        # 取得服務 (只會拿 dynamodb)
        self.__dynamodb = boto3.resource('dynamodb')
        # 取得資料表，查詢資料時會用到
        self.__table = boto3.resource('dynamodb').Table(self.table_name)
        # 取得連線，意味不明但就先放著
        self.__client = boto3.client('dynamodb')
        

        # 就是 key_schema
        self.key_schema = self.__table.key_schema


    def getSomeOneScoreList(self, player:str):
        response = self.__table.query(
            KeyConditionExpression=Key('player_name').eq(player)
        )
        return response

    # 檢查此玩家是否有在 table 內
    # 因為 update 就算沒有該人，也不會發生錯誤，故不實作。
    def checkSomeOneIsInTable(self):
        pass

    # UpdateItem
    # Edits an existing item's attributes, or adds a new item 
    # to the table if it does not already exist.
    # 更新某位玩家的分數
    # 不實作...
    def updateSomeOneScore(self, player:str, new_score:int):
        response = self.__table.update_item(
            Key={
                'player_name': player,
                'score': decimal.Decimal(new_score)
            },# Timestamp
            UpdateExpression="set score = :p",
            ExpressionAttributeValues={
                ':p': new_score
            },
            ReturnValues="UPDATED_NEW"
        )
        return response

       
    # 插入資料
    def put_item(self, player:str, score:int, info={}):
        response = self.__table.put_item(
            Item={
                'player_name': player,
                'score' : score,
                'info': info
            }
        )
        return response

    # 查看 table 建立時間。
    def getCreateTime(self):
        return self.__table.creation_date_time

    def addAttributeForSomeOne(self, player:str, NewAttribute:str, value:str):
        response = self.__table.update_item(
            Key={
                'player_name': player
            },
            UpdateExpression="set "+NewAttribute+" = :attr",
            ExpressionAttributeValues={
                ':attr': value
            },
            ReturnValues="UPDATED_NEW"
        )
        return response
        

    def getSomeOneItem(self, player:str):
        response = self.__table.query(
            KeyConditionExpression=Key('player_name').eq(player)
        )
        return response['Items'][0]

    def getALL(self, top=10):
        all_item = self.__table.scan()

        after_sorted =[]
        for i in all_item["Items"]:
            tmp = ( int(i['score']), 
                    { 'name'    :i['player_name'],
                      'country' :i['info']['country'],
                      'score'   : str(int(i['score']))
                    })
            after_sorted.append(tmp)
        return sorted(after_sorted,reverse=True)[:top]
        

    
    

