from datetime import datetime

def getTimestamp():
    return str(datetime.now().strftime("%d-%m-%Y_%H:%M:%S"))

