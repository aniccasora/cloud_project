# -*- coding:UTF-8 -*-
from flask import   Flask,\
                    request,\
                    jsonify,\
                    make_response,\
                    redirect,\
                    render_template,\
                    send_from_directory,\
                    url_for
from markupsafe import Markup, escape
import os
import boto3
from boto3.dynamodb.conditions import Key
import db_operator.worker as dbop
import json
from  DecimalEncoder import *
from datetime import datetime
import names
import random
import connexion
import socket

# 取得目前時間的寫法
dt_string = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")

# 新建一個 'dDB存取者' ，目標就是我們在 aws 上面建立的table name
worker = dbop.Dynamodb_Worker()

app = Flask(__name__)
app = connexion.FlaskApp(__name__, port=9090)

@app.route("/")
def home():
    
    host_ip = socket.gethostbyname(socket.gethostname())#str( request.host.split(':')[0] )
    return render_template("index.html",ip=host_ip)

@app.route("/score")
def score():

    top_list_sorted = worker.getALL()

    name_list = [ e[1]['name'] for e in [i for i in top_list_sorted] ]
    score_list = [ e[1]['score'] for e in [i for i in top_list_sorted] ]
    country_list = [ e[1]['country'] for e in [i for i in top_list_sorted] ]

        
    host_ip = str( request.host.split(':')[0] )
    
    print(name_list)
    print(score_list)
    print(country_list)

    return render_template("score.html",data = Markup(name_list),
                                        data2 = Markup(score_list),
                                        data3 = Markup(country_list),
                                        host_ip = host_ip)
    
@app.route('/api',methods= ['GET','POST'])
def api():
    data = request.get_json(force=True)

    print (data)
    print (type(data))
    print (data['name'])
    

    # data = json.dumps(data)
    # print (data)
    # print (type(data))
   
    # 準備資料
    this_info = {
            'timestamp' : str(datetime.now().strftime("%d-%m-%Y_%H:%M:%S")),
            'sex' : 'secret',
            'country' : data['country'],
            'msg' : 'YA'
    }
    
    global worker
    # # 插入一個row (item)
    response = worker.put_item( data['name'], decimal.Decimal(int(data['score'])), 
        info = this_info
    )

    print("PutItem succeeded:")
    print(json.dumps(response, indent=4, cls=DecimalEncoder))
    # 利用此種方式獲得 http response code
    print("response code =", response['ResponseMetadata']['HTTPStatusCode'])

    
    if data:
        return data
    else:
        return jsonify({'error' : 'Missing data!'})

#=================================

# 快速加入測試資料用
def makeFakeRecord():

    sex = ""
    score = -1
    name = ""
    countryList =  ["TW","JP","BE","DE","EG","ES","FR","GB","GR",\
                    "IS","JP","KR","KW","MN","US","VN","TW","TW","TW",\
                    "TW","TW","TW","TW","TW","TW","TW","TW","TW","TW"]
    countryIdx = random.randint(0,len(countryList)-1)
    country = countryList[countryIdx]

    # determine Male or Female by name.
    if (random.randint(0,1)):
        sex = "M"
        name = names.get_first_name(gender='male')
    else:
        sex = "F"
        name = names.get_first_name(gender='female')
   
    # key value
    score = random.randint(1000,9999)
    # name = {On the above}

    # other
    info = {
        'timestamp' : str(datetime.now().strftime("%d-%m-%Y_%H:%M:%S")),
        'sex' : sex,
        'country' : country,
        'msg' : 'Hello, World.'
    }

    return {'name':name,'score':score,'info':info}

if __name__=="__main__":

    app.run()
    #app.run(debug=True)
    
    '''
    # print(worker.getSomeOneScore('shiro'))



    # 製造假資料
    fakeRecord = makeFakeRecord()
    
    # # 插入一個row (item)
    response = worker.put_item( fakeRecord['name'], decimal.Decimal(fakeRecord['score']), 
        info = fakeRecord['info']
    )
    print("PutItem succeeded:")
    print(json.dumps(response, indent=4, cls=DecimalEncoder))
    # 利用此種方式獲得 http response code
    print("response code =", response['ResponseMetadata']['HTTPStatusCode'])
    
    # # 取得此人的 分數
    # response_dict = worker.getSomeOneItem('tony')
    # # 將擠在一坨的 json 格式排整齊 輸出。
    # print(json.dumps(response_dict,indent=4,cls=DecimalEncoder))
    
    # 更新某位玩家的分數，注意:如果 '沒有此玩家的話' 也會依照你給的東西插入資料。
    # response = worker.updateSomeOneScore("tonys", str(8000))
    # print("UpdateItem succeeded:")
    # print(json.dumps(response, indent=4, cls=DecimalEncoder))
    # print("response code =", response['ResponseMetadata']['HTTPStatusCode'])

    # 為一位玩家 加上 別的屬性，以及對應的屬性數值。
    # response = worker.addAttributeForSomeOne("shiro","record_time",\
    #     str(datetime.now().strftime("%d-%m-%Y_%H:%M:%S")))
    # print("UpdateItem succeeded:")
    # print(json.dumps(response, indent=4, cls=DecimalEncoder))
    # print("response code =", response['ResponseMetadata']['HTTPStatusCode'])
    '''