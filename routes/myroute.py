from flask import Flask
from flask import   Blueprint,\
                    send_from_directory
import os


simple_page = Blueprint('favicon_ico', __name__, template_folder='templates')
@simple_page.route('/favicon.ico')
def favicon(app):
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/comforable.ico')